'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
  const deleteMe = document.getElementById(`deleteMe`);
  deleteMe.remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wrapperList = document.querySelectorAll(`.wrapper`);

  wrapperList.forEach(item => {
    let counter = 0;
    item.querySelectorAll(`p`).forEach(paragraph => {
      counter += Number(paragraph.textContent)
      paragraph.remove()
    })
    item.innerHTML += `<p>${counter}<p>`;
  })
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
  const changeMe = document.getElementById(`changeMe`);
  changeMe.setAttribute(`type`, `text`);
  changeMe.setAttribute(`value`, `kek`);
}


/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li>
 *                                      <li>1</li>
 *                                      <li>2</li>
 *                                      <li>3</li>
 *
 */

function appendList() {
  const changeList = document.getElementById(`changeChild`);

  const li1 = document.createElement(`li`)
  li1.innerText = 3;
  changeList.appendChild(li1)

  const li2 = document.createElement(`li`)
  li2.innerText = 1;
  changeList.insertBefore(li2, changeList.children[1])
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  const itemList = document.querySelectorAll(`.item`);
  for (let i = 0; i < itemList.length; i++) {
    const item = itemList[i];

    if (item.classList.contains(`red`)) {
      item.classList.remove(`red`);
      item.classList.add(`blue`)
    }
    else {
      item.classList.remove(`blue`);
      item.classList.add(`red`);
    }
  }
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
