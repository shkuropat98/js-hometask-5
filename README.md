# js-hometask-5

## Ответьте на вопросы

1. Какие данные хранятся в следующих свойствах узла: `parentElement`, `children`, `previousElementSibling` и `nextElementSibling`?


    parentElement содержит ноду родителя

    children содержит коллекцию нодов потомков давнного элемента


    previousElementSibling - нода верхнего соседа(если по дереву то левого)

    nextElementSibling -  нода нижнего соседа(если по дереву то правого)

2. Нарисуй какое получится дерево для следующего HTML-блока.

```html
<p>Hello,<!--MyComment-->World!</p>
```

 Оно не очень растягивается
 и я нарисовал только p
 там по идее должна еще быть часть от доктайп -> хтмл.

 Получается если я ломаю верстку, она может не толькль криво отрендериться, но и будет создано больше узлов, значит рендериться она будет дольше
 
  
![tree](tree.png)

## Выполните задания

1. Клонируй репозиторий;
2. Допиши функции в `tasks.js`;
3. Для проверки работы функций открой index.html в браузере;
4. Создай MR с решением.
